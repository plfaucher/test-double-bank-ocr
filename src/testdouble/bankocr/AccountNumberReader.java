package testdouble.bankocr;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

import com.google.common.base.Splitter;
import com.google.common.collect.Iterables;

// TODO (1) separate Reading context from the parsing context
// TODO create BatchReader for bulk files
// TODO create AccountNumber class
//
// TODO implement US 2 in AccountNumberValidator
// TODO implement US 3 in separated parser from (1)
// TODO implement US 4 with MultilineNumberTranslator::findSimilarNumbers


public class AccountNumberReader {
    /**
     * each entry consists of 4 lines. The three first contains the data and the last one is a blank line.
     */
    private static final int LINES_PER_ENTRY = 4;
    /**
     * An account number is made up of 9 individual numbers
     */
    private static final int ACCOUNT_NUMBER_SIZE = 9;
    /**
     * Each line has 3 characters to represent a given number
     */
    private static final int CHARS_PER_LINE_NUMBER = 3;
    /**
     * 9 numbers times 3 chars gives us a line length of 27 characters per line
     */
    private static final int CHARS_PER_LINE = CHARS_PER_LINE_NUMBER * ACCOUNT_NUMBER_SIZE;
    /**
     * Total length of the string for validation
     */
    private static final int STRING_LENGTH = LINES_PER_ENTRY * CHARS_PER_LINE;

    /**
     * Entrypoint when reading a file
     * @param file path to the file containing the scan
     * @return the account number
     */
    public static String parseFromFile(String file) {
        String rawContent;
        try {
             rawContent = Files.readString(Path.of(file));
        } catch (IOException e) {
            return "ERR";
        }

        return parseFromString(rawContent);
    }

    public static String parseFromString(String string) {
        String sanitized = string.replace("\n","").replace("\r", "");
        isValidString(sanitized);
        // Remove the last line since it's basically useless for us. if batching we could separate by that line and kill two birds with one stone.
        sanitized = sanitized.substring(0, CHARS_PER_LINE * 3); //TODO (3) is a magic number -> extract

        List<String> splitLines = new LinkedList<>(Arrays.asList(Iterables.toArray(Splitter.fixedLength(CHARS_PER_LINE_NUMBER).split(sanitized), String.class)));

        String[] individualNumbers = assignCharsToNumbers(splitLines);


        return Arrays.stream(individualNumbers).map(MultilineNumberTranslator::translateMultiline).collect(Collectors.joining());
    }

    private static void isValidString(String string) {
        if (string.length() != STRING_LENGTH) {
            throw new IllegalArgumentException("Data received is not in the correct format");
        }
    }

    /**
     * Entry point for the recursive method.
     * Given a list of split Strings, return an array the size of an account number.
     * Each element contains all the "data points" required to evaluate an individual number in the account number.
     *
     * @param splitLines a list of split Strings
     * @return each number of the account number correctly sorted
     * @see AccountNumberReader#assignCharsToNumbers
     */
    private static  String[] assignCharsToNumbers(List<String> splitLines) {
        String[] acc = new String[ACCOUNT_NUMBER_SIZE];
        Arrays.fill(acc, "");

        return assignCharsToNumbers(splitLines, acc, 0);
    }

    /**
     * Recursive method to solve a big String representing an account number of the form 123456789
     *
     *
     * @param splitLines input lines split
     * @param acc String array that serves as the accumulator for the answer
     * @param current the index
     * @return each number of the account number correctly sorted
     */
    private static  String[] assignCharsToNumbers(List<String> splitLines, String[] acc, int current) {
        if (splitLines.isEmpty()) {
            return acc;
        }

        acc[current] = acc[current] + splitLines.remove(0);
        return assignCharsToNumbers(splitLines, acc, ++current % ACCOUNT_NUMBER_SIZE);
    }


}
