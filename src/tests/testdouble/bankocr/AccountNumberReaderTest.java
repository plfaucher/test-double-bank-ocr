package testdouble.bankocr;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class AccountNumberReaderTest {

    @Test public void readingFile_nineZeroes_shouldReturnNineZeroes(){
        assertEquals(AccountNumberReader.parseFromFile("resources/samples/000000000.txt"), "000000000");
    }

    @Test public void reading_nineZeroes_shouldReturnNineZeroes() {
        String useCase = " _  _  _  _  _  _  _  _  _ \n" +
                         "| || || || || || || || || |\n" +
                         "|_||_||_||_||_||_||_||_||_|\n" +
                         "                           ";

        assertEquals(AccountNumberReader.parseFromString(useCase), "000000000");
    }

    // TODO multiline string is borked. repaste from
    @Test public void reading_nineOnes_shouldReturnNineOnes() {
        String useCase = """
                                        \s
                |  |  |  |  |  |  |  |  |
                |  |  |  |  |  |  |  |  |
                                        \s""".indent(6);

        assertEquals(AccountNumberReader.parseFromString(useCase), "111111111");
    }

    @Test public void reading_nineTwos_shouldReturnNineTwos() {
        String useCase = """
                 _  _  _  _  _  _  _  _  _\s
                 _| _| _| _| _| _| _| _| _|
                |_ |_ |_ |_ |_ |_ |_ |_ |_\s
                                          \s""".indent(4);
        assertEquals(AccountNumberReader.parseFromString(useCase), "222222222");
    }

    @Test public void reading_nineThrees_shouldReturnNineThrees() {
        String useCase = """
                _  _  _  _  _  _  _  _  _\s
                _| _| _| _| _| _| _| _| _|
                _| _| _| _| _| _| _| _| _|
                                         \s""".indent(5);
        assertEquals(AccountNumberReader.parseFromString(useCase), "333333333");
    }

    @Test public void reading_nineFours_shouldReturnNineFours() {
        String useCase = """
                                          \s
                |_||_||_||_||_||_||_||_||_|
                  |  |  |  |  |  |  |  |  |
                                          \s""".indent(4);
        assertEquals(AccountNumberReader.parseFromString(useCase), "444444444");
    }

    @Test public void reading_nineFives_shouldReturnNineFives() {
        String useCase = """
                 _  _  _  _  _  _  _  _  _\s
                |_ |_ |_ |_ |_ |_ |_ |_ |_\s
                 _| _| _| _| _| _| _| _| _|
                                          \s""".indent(4);
        assertEquals(AccountNumberReader.parseFromString(useCase), "555555555");
    }

    @Test public void reading_nineSixes_shouldReturnNineSixes() {
        String useCase = """
                 _  _  _  _  _  _  _  _  _\s
                |_ |_ |_ |_ |_ |_ |_ |_ |_\s
                |_||_||_||_||_||_||_||_||_|
                                          \s""".indent(4);
        assertEquals(AccountNumberReader.parseFromString(useCase), "666666666");
    }

    @Test public void reading_nineSevens_shouldReturnNineSevens() {
        String useCase = """
                _  _  _  _  _  _  _  _  _\s
                 |  |  |  |  |  |  |  |  |
                 |  |  |  |  |  |  |  |  |
                                         \s""".indent(5);
        assertEquals(AccountNumberReader.parseFromString(useCase), "777777777");
    }

    @Test public void reading_nineEights_shouldReturnNineEights() {
        String useCase = """
                 _  _  _  _  _  _  _  _  _\s
                |_||_||_||_||_||_||_||_||_|
                |_||_||_||_||_||_||_||_||_|
                                          \s""".indent(4);
        assertEquals(AccountNumberReader.parseFromString(useCase), "888888888");
    }

    @Test public void reading_nineNines_shouldReturnNineNines() {
        String useCase = """
                 _  _  _  _  _  _  _  _  _\s
                |_||_||_||_||_||_||_||_||_|
                 _| _| _| _| _| _| _| _| _|
                                          \s""".indent(4);
        assertEquals(AccountNumberReader.parseFromString(useCase), "999999999");
    }

    @Test public void reading_123456789_shouldReturn123456789() {
        String useCase = """
                  _  _     _  _  _  _  _\s
                | _| _||_||_ |_   ||_||_|
                ||_  _|  | _||_|  ||_| _|
                                        \s""".indent(6);
        assertEquals(AccountNumberReader.parseFromString(useCase), "123456789");
    }

    //TODO Implement tests for other uses cases
}
