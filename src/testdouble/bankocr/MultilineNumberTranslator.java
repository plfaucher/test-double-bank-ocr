package testdouble.bankocr;

import java.util.Map;

public class MultilineNumberTranslator {
    public static final String NOT_FOUND = "?";

    public static final String ZERO_MULTILINE = " _ " +
                                                "| |" +
                                                "|_|";

    public static final String ONE_MULTILINE = "   " +
                                               " | " +
                                               " | ";

    public static final String TWO_MULTILINE = " _ " +
                                               " _|" +
                                               "|_ ";

    public static final String THREE_MULTILINE = " _ " +
                                                 " _|" +
                                                 " _|";

    public static final String FOUR_MULTILINE = "|_|" +
                                                "  |" +
                                                "   ";

    public static final String FIVE_MULTILINE = " _ " +
                                                "|_ " +
                                                " _|";

    public static final String SIX_MULTILINE = " _ " +
                                               "|_ " +
                                               "|_|";

    public static final String SEVEN_MULTILINE = " _ " +
                                                 "  |" +
                                                 "  |";

    public static final String EIGHT_MULTILINE = " _ " +
                                                 "|_|" +
                                                 "|_|";

    public static final String NINE_MULTILINE = " _ " +
                                                "|_|" +
                                                " _|";

    /**
     * A map of a three-line number and its standard form
     */
    private static final Map<String, String> translated;

    /**
     * A map of a number and its similar numbers when given uncertainty about the scanning process
     */
    private static final Map<String, String[]> similarNumbers;

    static {
        translated = Map.of(
                ZERO_MULTILINE, "0",
                ONE_MULTILINE, "1",
                TWO_MULTILINE, "2",
                THREE_MULTILINE, "3",
                FOUR_MULTILINE, "4",
                FIVE_MULTILINE, "5",
                SIX_MULTILINE, "6",
                SEVEN_MULTILINE, "7",
                EIGHT_MULTILINE, "8",
                NINE_MULTILINE, "9"
        );

        similarNumbers = Map.of(
                "1", new String[] {"7"} //TODO etc.
        );
    }

    /**
     * Translate a three-line number into its standard form
     * @param multilineNumber a String representing a number
     * @return the number as a string
     */
    public static String translateMultiline(String multilineNumber) {
        return translated.getOrDefault(multilineNumber, NOT_FOUND);
    }

    /**
     * Find similar numbers for a given number
     * @param string the number
     * @return similar numbers
     */
    public static String[] findSimilarNumbers(String string) {
        return similarNumbers.getOrDefault(string, new String[]{});
    }
}
